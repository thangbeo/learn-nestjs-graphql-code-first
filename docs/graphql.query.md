# Write your query or mutation here example
query Coffees {
  coffees {
    id
    name
  }
}

mutation {
  createCoffee(createCoffeeInput: {
    name: "Cf",
    brand: "G7",
    flavors: ["f1"]
  }) {
    name
  }
}


mutation {
  updateCoffee(id: 2, updateCoffeeInput: {
    name: "Cafe hoa tan 2 update"
  }) {
    name
  }
}

mutation {
  removeCoffee(id: 2) {
    name
  }
}


query Drinks {
    drinks {
        name
    }
}

query Drinks {
    drinks {
        name
        ... on Coffee {
            name
            id
            brand
            createdAt
        }
        ... on Tea {
            name
        }
    }
}


query Drinks {
    drinks {
        ... on Coffee {
            name
            id
            brand
            createdAt
        }
        ... on Tea {
            name
        }
    }
}


mutation CreateCoffee {
    createCoffee(
        createCoffeeInput: { name: "G83", brand: "TN", flavors: "['f1']", type: ARABICA }
    ) {
        name
        id
        brand
        createdAt
        type
        flavors {
            id
            name
        }
    }
}


subscription CoffeeAdded {
    coffeeAdded {
        name
        id
        brand
        createdAt
        type
    }
}

